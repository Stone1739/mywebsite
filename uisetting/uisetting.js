;(function( $, undefined ) {
	"use strict";
	/**
	 * @description
	 * Used to set user interface by getting html document.
	 * Supported drop-down list.
	 * 
	 * @param {object} options - Configuration object
	 * @param {string} options.type - Type of UI.
	 * @param {object} options.data - The data to be set into the UI.
	 * @param {JQuery obj} options.style - Html sytle of UI. Can be set by user or fetch by system.
	 * @param {string} options.labelid - The label id which to be fetch and set in options.style.
	 * @param {string} options.listid - The dropdown list id which to be fetch and set in options.style.
	 * @param {string} checkboxid - The checkbox id which to be fetch and set in options.style.
	 * @param {object array} controller
	 *
	 *  @example
	 * 		options = {
	 * 			type: "select",
	 * 			data: [
	 * 					{
	 * 						"checked": true,
	 * 						"subtitle": "label_1",
	 * 						"content": "content_1"
	 * 					},
	 * 					{
	 * 						"checked": false,
	 * 						"subtitle": "label_2",
	 * 						"content": "content_2"
	 * 					}
	 * 			  	],
	 * 			labelid: "defaultLabel",
	 * 			listid: "defaultList"
	 * 		}
	 */

	var UIsetting = function(options) {
		var panel = this;

		var defaults = { 
			type: "none",	// type of UI
			data: {},	// UI data
			style: this.children().clone(),
			labelid: "defaultLabel",
			listid: "defaultList",
			checkboxid: "defaultcheck"
		};

		// all type of UI, used for switch
		var alltype = {
			ddl: "ddlist",
			chk: "checkbox"
		};

		var setting;
		var style;
		var type;
		var data;
		var controller = [];

		function init(oOpts){
			setting = $.extend({}, defaults, oOpts);
			
			// use div to wrap style for using jquery function "find"
			style = $("<div></div>").append(setting.style);
			type = setting.type;
			data = setting.data;

			panel.data({uistyle: style});
			
			panel.empty();
			controller = [];

			setdata(data);
		}

		/**
		 * Set cascade dropdown list with recursive.
		 * @param  {JQuery obj} - style Html sytle of UI.
		 * @param  {object array} data - cascade dropdown list data.
		 * @param  {string array} data.title - the label of each list.
		 * @param  {object} data.data - the content of cascade list.
		 * @param  {string array} data.selected - (optional)
		 *                        the default selected option of list. 
		 *
		 *	@example
		 * 		data = [
		 * 			{
		 * 				"title": [
		 * 					"title_1",
		 * 					"title_2"
		 * 				],
		 * 				"list": {
		 * 					"content_1 (layer_1)": [
		 * 						"content_1-1 (layer_2)",
		 * 						"content_1-2 (layer_2)"
		 * 					],
		 * 					"content_2 (layer_1)": [
		 * 						"content_2-1 (layer_2)",
		 * 						"content_2-2 (layer_2)",
		 * 						"content_2-3 (layer_2)"
		 * 					]
		 * 				}
		 * 			},
		 * 			{
		 * 				"title": [
		 * 					"title_3",
		 * 					"title_4",
		 * 					"title_5"
		 * 				],
		 * 				"selected": [
		 * 					"content_1",
		 * 					"content_1-2",
		 * 					"content_1-2-2"
		 * 				],
		 * 				"list": {
		 * 					"content_1 (layer_1)": {
		 * 						"content_1-1 (layer_2)": [
		 * 							"content_1-1-1 (layer_3)",
		 * 							"content_1-1-2 (layer_3)",
		 * 							"content_1-1-3 (layer_3)"
		 * 						],
		 * 						"content_1-2 (layer_2)": [
		 * 							"content_1-2-1 (layer_3)",
		 * 							"content_1-2-2 (layer_3)",
		 * 							"content_1-2-3 (layer_3)"
		 * 						]
		 * 					}
		 * 				}
		 * 			}
		 * 		]
		 */
		function setList(listdata){
			panel.empty();
			
			var labelid = "#" + setting.labelid;
			var listid = "#" + setting.listid;

			for(var listidx in listdata){
				var thislistDataObj = listdata[listidx];
				var datalength = thislistDataObj["title"].length; // the amount of cascade listdata layer
				var useDefault = false; // use default selected

				// check if default selected is used and the length is same to the title.
				if(listdata[listidx].default){
					if(listdata[listidx].default.length === datalength){
						useDefault = true;
					}else{
						console.error("The length is not same");
					}
				}else{
					thislistDataObj["default"] = [];
					useDefault = false;
				}

				var thislistData = thislistDataObj["list"];
				for(var layer = 0; layer < datalength ; layer++){
					var $newContent = style.clone();
					var $newLabel = $newContent.find(labelid);
					var $newList = $newContent.find(listid);

					// Check if selected value is exist in list
					var itemInList = false;

					$newList.empty();

					// change id
					$newLabel.attr("id", setting.labelid + "_" + listidx + "_" + layer);
					$newList.attr("id", setting.listid + "_" + listidx + "_" + layer);

					$newLabel.text(thislistDataObj["title"][layer]);

					// Use object or array to check if the data is the end.
					if(Array.isArray(thislistData)){ // end of data
						for(var item in thislistData) {
							$newList.append($('<option/>').val(thislistData[item]).html(thislistData[item]));

							// Check if selected value is exist in list
							if(useDefault && thislistDataObj["default"][layer] === thislistData[item]){
								$newList.val(thislistDataObj["default"][layer]);
								itemInList = true;
							}
						}
					}else{
						for(var property in thislistData) {
							$newList.append($('<option/>').val(property).html(property));

							if(useDefault && thislistDataObj["default"][layer] === property){
								$newList.val(thislistDataObj["default"][layer]);
								itemInList = true;
							}
						}
					}

					// if selected value is not exist in list
					if(useDefault && !itemInList){
						console.error("The setting default option is not found in dropdown list.");
					}

					panel.append($newContent.children());

					// Record data
					controller.push({
						panel: $newContent,
						label: $newLabel,
						list: $newList
					});

					// use node to chain cascade dropdown list
					$newList.data({
						listdata: thislistData,
						next: null
					});

					// set cascade list use recursive
					if(layer > 0){
						var $parent = $("#" + setting.listid + "_" + listidx + "_" + (layer-1));
						
						// save next layer with data function
						$parent.data("next", $newList);

						// set listener
						$parent.change(updataCascade);
					}

					// next layer data
					thislistData = thislistData[$newList.find(":selected").text()];
				}
			}
		}

		function updataCascade(){
			var next = $(this).data().next;
			var newdata = $(this).data().listdata[$(this).find(":selected").text()];
			setCascadeDropdown(next, newdata);

			function setCascadeDropdown(next, newdata){
				if(next){
					next.data().listdata = newdata;

					// clear next layer dropdown list
					next.empty();

					// if is array then is the end of cascade list
					if(Array.isArray(newdata)){
						for(var item in newdata) {
							next.append($('<option/>').val(newdata[item]).html(newdata[item]));
						}
					}else{
						for(var property in newdata) {
							next.append($('<option/>').val(property).html(property));
						}

						// has next because is object
						setCascadeDropdown(next.data().next, newdata[next.find(":selected").text()]);
					}
				}
			}
		}

		function setcheckbox(data){
			var labelid = "#" + setting.labelid;
			var checkboxid = "#" + setting.checkboxid;
			var lblcounter = 0;

			for(var idx in data){
				var subtitle = data[idx].subtitle;
				var content = data[idx].content;

				var newContent = style.clone();
				var $newLabel = newContent.find(labelid);
				var $newCheckbox = newContent.find(checkboxid);

				if(data[idx].checked){
					$newCheckbox.prop('checked', true);
				}

				if(!data[idx].subtitle){
					$newLabel.html(content);
				}else{
					$newLabel.empty();
					$newLabel.html("<b>" + subtitle + "</b>" + " " + content);
				}

				controller.push({
					label:$newLabel,
					subtitle:subtitle,
					content:content,
					checkbox:$newCheckbox
				});
				
				var newlabelId = $newLabel.attr("id") + "_" + lblcounter;
				var newckeckboxId = $newCheckbox.attr("id") + "_" + lblcounter;

				$newLabel.attr("id", newlabelId);
				$newCheckbox.attr("id", newckeckboxId);

				panel.append(newContent);
				lblcounter++;
			}
		}

		function setdata(data){
			switch(type){
				default:
				var errormsg = "No type be set or type is not supported.\n" + 
						"Type: " + type + "\n" + Error().stack;
					console.error(errormsg);
					return;
				break;
				case alltype.ddl:
					setList(data);
				break;
				case alltype.chk:
					setcheckbox(data);
				break;
			}
		}

		function getstatus(){
			switch(type){
				default:
					console.error("No type be set.\n" + Error().stack);
					return;
				break;
				case alltype.chk:
					var data = [];
					$.each(controller, function(idx, value){
						data.push({
							checked: value.checkbox.is(":checked") ? true : false,
							text: value.label.text(),
							subtitle: value.subtitle,
							content: value.content
						});
					});
					return data;
				break;
			}
		}

		function getSelected(){
			var data = {};
			for(var idx in controller){
				data[controller[idx].label.text()]
					 = controller[idx].list.find(":selected").text();
			}
			return data;
		}

		/**
		 * [setSelected description]
		 * @param {string array} selectedList - value that used to be set into ddlist.
		 *                       				can be null if the value doesn't want
		 *                       				to be changed.
		 *
		 * @example
		 * 		selectedList = [
		 * 			"",
		 * 			"",
		 * 			"new value",
		 * 			""
		 *   	]
		 */
		function setSelected(selectedList){
			for(var idx in controller){
				if(selectedList[idx]){
					controller[idx].list.val(selectedList[idx]);
					if(!controller[idx].list.val()){
						var errmsg = "The setting value of list is not exist\n" + 
							"label: " + controller[idx].label.text() + ", " + 
							"setting value: " + selectedList[idx] + "\n";

						console.error(errmsg);
					}
					controller[idx].list.trigger("change");
				}
			}
		}

		function getLabel(){
			var data = [];
			for(var idx in controller){
				data.push(controller[idx].label.text());
			}
			return data;
		}

		function getPanelStyle(){
			return panel.children().clone();
		}

		function getListObj(){
			var listObj = [];
			for(var idx in controller){
				listObj.push(controller[idx].list);
			}
			return listObj;
		}

		init(options);
		
		var publicFunc = {};
		switch(type){
			default:
				console.error("No type be set.\n" + Error().stack);
				return;
			break;
			case alltype.ddl:
				publicFunc = $.extend(true, publicFunc, {
					getLabel,
					getListObj,
					getSelected,
					setList,
					setSelected
				});
			break;
			case alltype.chk:
				publicFunc = $.extend(true, publicFunc, {
					getstatus,
					setdata
				});
			break;
		}
		return publicFunc = $.extend(true, publicFunc, {
			getPanelStyle
		});
	};

	$.fn.uisetting = UIsetting;

	return $.fn.uisetting;
}( jQuery ));