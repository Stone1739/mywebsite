function initialize(){

	var listArea = {
		"藝術": "art",
		"自然科學": "science"
	};
	var listEduStep = {
		"小學": "elementary",
		"中學": "junior", 
		"高中": "senior"
	};
	var listLearningStep = {
		"階段一": "step1",
		"階段二": "step2", 
		"階段三": "step3"
	};
	var listSeniorType = {
		"高中": "1",
		"五專": "2", 
		"綜合高中": "3", 
		"高職": "4"
	};
	var listSubject = {
	};

	setList($("#menu_area"), listArea);
	setList($("#menu_eduStep"), listEduStep);
	setList($("#menu_learningStep"), listLearningStep);
	setList($("#menu_seniorType"), listSeniorType);
	setList($("#menu_subject"), listSubject);

	$("#menu_area").change(function(){
		menuChange();
	});

	$("#menu_eduStep").change(function(){
		menuChange();
	});

	menuChange();
	$("#checklist").hide();
	$("#courseOutline").hide();
}

function menuChange(){
	switch($("#menu_area").val()){
		case "art":
			switch($("#menu_eduStep").val()){
				case "elementary":
					listSubject = {
						"音樂": "音樂",
						"視覺藝術": "視覺藝術",
						"表演藝術": "表演藝術"
					};
					break;
				case "junior":
					listSubject = {
						"音樂": "音樂",
						"視覺藝術": "視覺藝術",
						"表演藝術": "表演藝術"
					};
					break;
				case "senior":
					listSubject = {
						"音樂":	"音樂",
						"美術":	"美術",
						"藝術生活":	"藝術生活",
						"表演創作":	"表演創作",
						"基本設計":	"基本設計",
						"多媒體音樂":	"多媒體音樂",
						"新媒體藝術":	"新媒體藝術"
					};
					break;
			}
			setList($("#menu_subject"), listSubject);
			$("#lblsubject").text("學科名稱");
			$("#menu_subject").show();
			$("#theme").hide();
			$("#subtheme").hide();
			$("#express_item").hide();
			$("#express_subitem").hide();
			break;
		case "science":
			switch($("#menu_eduStep").val()){
				case "elementary":
					$("#lblsubject").text("跨科概念");
					$("#subject").show();
					$("#express_item").hide();
					$("#express_subitem").hide();
					break;
				case "junior":
					$("#subject").hide();
					$("#express_item").show();
					$("#express_subitem").show();
					break;
				case "senior":
					$("#lblsubject").text("科別");
					$("#subject").show();
					$("#express_item").show();
					$("#express_subitem").show();
					break;
			}
			$("#theme").show();
			$("#subtheme").show();
			break;
	}

	switch($("#menu_eduStep").val()){
		case "elementary":
			listLearningStep = {
				"階段一": "step1",
				"階段二": "step2",
				"階段三": "step3"
			};
			break;
		case "junior":
			listLearningStep = {
				"階段四": "step4"
			};
			break;
		case "senior":
			listLearningStep = {
				"階段五": "step5"
			};
			break;
	}
	setList($("#menu_learningStep"), listLearningStep);
	if($("#menu_eduStep").val() == "senior"){
		$("#seniorType").show();
	}else{
		$("#seniorType").hide();
	}
}

function setList(select, list){
	select.empty();
	$.each(list, function(key,value) {
		select.append($("<option></option>").attr("value", value).text(key));
	});
}

function funcSearch() {	
	// 測試用
	$("#testarea").html("Search:<br/>" 
		+ $("#menu_area").val() + "<br/>"
		+ $("#menu_eduStep").val() + "<br/>"
		+ $("#menu_learningStep").val() + "<br/>"
		+ $("#menu_seniorType").val() + "<br/>"
		+ $("#menu_subject").val() + "<br/>");

	// request
	$.ajax({
		dataType: "json",
		url: "test.json",
		success: function(data){
			$("#checkExpress").empty();
			$.each( data, function( key, val ) {
				var str = '<div class="form-group">' +
						'<label class="custom-control custom-checkbox">' +
							'<input type="checkbox" class="custom-control-input">' +
							'<span class="custom-control-indicator"></span>' +
							'<span class="custom-control-description">' +
								'<span class="keyCode">' + key + '</span>' +
								'<span>' + val + '</span>' +
							'</span>' +
						'</label>' +
					'</div>';
			});
		}, 
		error: function(err){
			console.log("Get json failed.");
			console.log(err);
		}
	}).done(function(){
		$("#checkExpress").append(str);
		$("#checklist").show();
	});
}

function funcSubmit() {
	var str = "";
	$("input:checked ~ span.custom-control-description > span.keyCode").each(function(){
		str = str + "<br/>" + $(this).html();
	})
	$("#testarea").html("Submit:" + str);

	$("#courseOutline").show();
}

function funcClear() {
	$("#testarea").text("clear");
	$("#menu_area").val(0);
	$("#menu_eduStep").val(0);
	$("#menu_learningStep").val(0);
	$("#menu_seniorType").val(0);
	$("#menu_subject").val(0);
}