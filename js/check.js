function initialize(){
	/**
	 * 定義form中個別的標籤並組成物件
	 *
	 * Html id 命名規則：
	 * 項目：lbl
	 * 內容：text
	 * 整個區塊：pnl
	 * 個別的名稱開頭大寫，ex:pnlId
	 */

	// tag id form Html
	var tagList = {
		id: "Id",
		mail: "Mail",
		school: "School",
		eduStep: "EduStep",
		area: "Area",
		subject: "Subject",
	};

	// Item object in form group
	function Group(name){
		//private
		var panel = $('#pnl' + name);
		var label = $('#lbl' + name);
		var content = $('#text' + name);

		//public
		this.getLabel = function(){
			return label.text();
		}

		this.setLabel = function(newlabel){
			label.text(newlabel);
		}

		this.getContent = function(){
			return content.val();
		}

		this.setContent = function(newcontent){
			content.val(newcontent);
		}
	}

	var id = new Group(tagList.id);
	var mail = new Group(tagList.mail);
	var school = new Group(tagList.school);
	var eduStep = new Group(tagList.eduStep);
	var area = new Group(tagList.area);
	var subject = new Group(tagList.subject);

	function GroupTest(objGroup){
		console.log('objGroup test');
		console.log('Get label ' + objGroup.getLabel());
		console.log('Get content ' + objGroup.getContent());
	}

	GroupTest(id);
	GroupTest(mail);
	GroupTest(school);
	GroupTest(eduStep);
	GroupTest(area);
	GroupTest(subject);
}